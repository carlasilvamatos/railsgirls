require 'rails_helper'

RSpec.describe Idea, type: :model do
  it 'has a name' do # yep, you can totally use 'it'
    idea = Idea.create!(name: 'My Awesome Idea Name') # creating a new idea 'instance'
    expect(idea.name).to eq('My Awesome Idea Name') # this is our expectation
  end

  it 'has a description' do
    idea = Idea.create!(description: 'My Idea gets a description') # creating a new description 'instance'
    expect(idea.description).to eq('My Idea gets a description') # this is my expectation
  end
end
